using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public Transform rotateAxis;
    [SerializeField] Transform[] childBlocks;
    public bool isSelect = false;

    bool isDown = false;
    bool isFloor = false;
    float speed = 0.8f;

    public BlockFactory blockFactory;

    enum MoveType
    {
        Move,
        Rotate
    }

    CheckMoving checkMoving;

    private void Start()
    {
        checkMoving = GetComponent<CheckMoving>();
        StartCoroutine(DownMove(speed));
    }

    void Update()
    {
        if (isSelect)
        {
            GetInputs();
        }
    }

    IEnumerator DownMove(float time)
    {
        while (isSelect)
        {
            MoveBlock(Vector3.down);
            yield return new WaitForSeconds(time);
        }
    }

    private void GetInputs()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MoveBlock(Vector3.down);
            isDown = true;
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MoveBlock(Vector3.right);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MoveBlock(Vector3.left);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            RotateBlock();
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            FallDown();
        }
    }

    private void FallDown()
    {
        int maxL = 0;
        foreach(Transform t in childBlocks)
        {
            int tempL = checkMoving.GetFloorY(checkMoving.ToMapX(t.position.x));
            if (maxL < tempL)
            {
                maxL = tempL;
            };
        }

        while (true)
        {
            maxL--;
            if (MoveBlock(Vector3.down * (maxL)))
            {
                break;
            };
        }
    }

    private bool MoveBlock(Vector3 moveDir)
    {
        if (checkMoving.CheckNextMove(childBlocks, moveDir))
        {
            transform.Translate(moveDir);
            SetMyPos(childBlocks);

            if (!isFloor)
            {
                isFloor = checkMoving.CheckFloor(childBlocks, () => StartFixBlock());
            }
            return true;
        }
        return false;
    }

    private void RotateBlock()
    {
        if (checkMoving.CheckNextRotate(childBlocks, 90f))
        {
            rotateAxis.Rotate(Vector3.forward, 90);
            SetMyPos(childBlocks);
        }

        if (!isFloor)
        {
            isFloor = checkMoving.CheckFloor(childBlocks, () => StartFixBlock());
        }
    }

    private void StartFixBlock()
    {
        StartCoroutine(FixBlock());
    }

    IEnumerator FixBlock()
    {
        yield return new WaitForSeconds(1);
        blockFactory.isFixed = true;
        isSelect = false;
        //ClearMe() 바닥감지하고 떨어졌을때 어떻게 할건G
        CheckHorizontalLine();
    }

    private void CheckHorizontalLine()
    {
        Debug.Log("CheckHorizontalLine");

        for (int j = 0; j < GameManager.instance.GetMapHeight(); j++)
        {
            bool isFull = true;
            for (int i = 0; i < GameManager.instance.GetMapWidth(); i++)
            {
                if (!GameManager.instance.GetMap(i, j))
                {
                    isFull = false;
                    break;
                }
            }

            if (isFull)
            {
                StartCoroutine(ClearHorizontalLine(j));
            }
        }
    }

    IEnumerator ClearHorizontalLine(int clearLine)
    {
        Debug.Log("ClearHorizontalLine");
        for (int i = 0; i < GameManager.instance.GetMapWidth(); i++)
        {
            GameManager.instance.SetMap(i, clearLine, false);
        }
        yield return new WaitForSeconds(1);
        RaycastHit[] hits;

        hits = Physics.RaycastAll(new Vector3(-1, clearLine + 0.5f, 0), Vector3.right, 11, 1 << LayerMask.NameToLayer("Block"));

        for (int n = 0; n < hits.Length; ++n)
        {
            Debug.Log("Destroy : " + hits[n].transform.name);
            Destroy(hits[n].collider.gameObject);
        }
        yield return new WaitForEndOfFrame();
        RelocateBlocks(clearLine);
    }

    private void RelocateBlocks(int clearLine)
    {
        for (int j = clearLine + 1; j < GameManager.instance.GetMapHeight(); ++j)
        {
            for (int i = 0; i < GameManager.instance.GetMapWidth(); i++)
            {
                GameManager.instance.SetMap(i, j - 1, GameManager.instance.GetMap(i, j));
            }
        }
        MoveDownBlocks(clearLine);
    }

    private void MoveDownBlocks(int clearLine)
    {
        Transform[] blocks = GameManager.instance.GetComponentsInChildren<Transform>();
        foreach (Transform t in blocks)
        {
            if(t.CompareTag("Cube") && t.position.y > clearLine)
            {
                t.Translate(Vector3.down, Space.World);
            }
            else if (t.CompareTag("BlockParent") && t.GetChild(0).childCount == 0)
            {
                Destroy(t.gameObject);
            }
        }    
    }


  




    public void SetMyPos(Transform[] childBlocks)
    {
        for (int i = 0; i < childBlocks.Length; ++i)
        {
            int mapX = Mathf.FloorToInt(childBlocks[i].position.x);
            int mapY = Mathf.FloorToInt(childBlocks[i].position.y);
            GameManager.instance.SetMap(mapX, mapY, true);
        }
    }


}
