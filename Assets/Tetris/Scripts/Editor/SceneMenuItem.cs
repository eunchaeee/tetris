using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class SceneMenuItem : MonoBehaviour
{
    [MenuItem("Scene/Login")]
    static void OpenLoginScene()
    {
        EditorSceneManager.OpenScene("Assets/Scenes/Login.unity");
    }

    [MenuItem("Scene/Intro")]
    static void OpenMainScene()
    {
        EditorSceneManager.OpenScene("Assets/Scenes/Intro.unity");
    }

    [MenuItem("Scene/Game")]
    static void OpenGameScene()
    {
        EditorSceneManager.OpenScene("Assets/Scenes/Game.unity");
    }
}
