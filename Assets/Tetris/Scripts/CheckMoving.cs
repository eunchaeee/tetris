using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckMoving : MonoBehaviour
{
    public int GetFloorY(int x)
    {
        for(int y = GameManager.instance.GetMapHeight() - 1; y > 0; --y)
        {
            if(GameManager.instance.GetMap(x, y))
            {
                return y;
            }
        }
        return 0;
    }

    public int ToMapX(float posX)
    {
        return Mathf.FloorToInt(posX);
    }

    public int ToMapY(float posY)
    {
        return Mathf.FloorToInt(posY);
    }

      // map process
    public bool CheckNextMove(Transform[] childBlocks, Vector3 nextMove)
    {
        List<(int, int)> tempCoords = ClearMe(childBlocks);

        for (int i = 0; i < childBlocks.Length; ++i)
        {
            Vector3 nextPos = childBlocks[i].position + nextMove;

            int mapX = ToMapX(nextPos.x);
            int mapY = ToMapY(nextPos.y);

            if (mapX < 0 || mapX > GameManager.instance.GetMapWidth() - 1)
            {
                RecoverMe(tempCoords);
                return false;
            }
            else if (mapY < 0)
            {
                RecoverMe(tempCoords);
                return false;
            }
            else if (GameManager.instance.GetMap(mapX, mapY))
            {
                RecoverMe(tempCoords);
                return false;
            }
        }
        ClearMe(childBlocks);
        return true;
    }

    public bool CheckNextRotate(Transform[] childBlocks, float rotateAngle)
    {
        List<(int, int)> tempCoords = ClearMe(childBlocks);

        for (int i = 0; i < childBlocks.Length; ++i)
        {
            Vector3 dir = childBlocks[i].position - transform.position;
            float newX = dir.x * Mathf.Cos(rotateAngle) - dir.y * Mathf.Sin(rotateAngle);
            float newY = dir.x * Mathf.Sin(rotateAngle) + dir.y * Mathf.Cos(rotateAngle);
            Vector3 nextPos = new Vector3(newX, newY, 0) + transform.position;
            int mapX = ToMapX(nextPos.x);
            int mapY = ToMapY(nextPos.y);
            //Debug.Log("CheckNextRotate" + transform.position + transform.name + transform.parent.position);
            if (mapX < 0 || mapX > GameManager.instance.GetMapWidth() - 1)
            {
                RecoverMe(tempCoords);
                //Debug.Log(1 + "," + mapX + "," + GameManager.instance.GetMapWidth());
                return false;
            }
            else if (mapY < 0)
            {
                //Debug.Log(2);
                RecoverMe(tempCoords);
                return false;
            }
            else if (GameManager.instance.GetMap(mapX, mapY))
            {
                //Debug.Log(3);
                RecoverMe(tempCoords);
                return false;
            }
        }
        ClearMe(childBlocks);
        return true;
    }


    public bool CheckFloor(Transform[] blockParent, Action onFloor)
    {
        bool check = false;
        List<(int, int)> tempCoords = ClearMe(blockParent);
        for (int i = 0; i < blockParent.Length; ++i)
        {
            Vector3 childPos = blockParent[i].transform.position;
            int mapX = ToMapX(childPos.x);
            int mapY = ToMapY(childPos.y);
            if (mapY == 0 || GameManager.instance.GetMap(mapX, mapY - 1))
            {
                check = true;
                break;
            }
        }

        RecoverMe(tempCoords);
        if (check) { onFloor?.Invoke(); }
        return check;
    }

    public List<(int, int)> ClearMe(Transform[] childBlocks)
    {
        List<(int, int)> clearedBlocks = new List<(int, int)>();

        for (int i = 0; i < childBlocks.Length; ++i)
        {
            int mapX = ToMapX(childBlocks[i].position.x);
            int mapY = ToMapY(childBlocks[i].position.y);
            GameManager.instance.SetMap(mapX, mapY, false);
            clearedBlocks.Add((mapX, mapY));
        }

        return clearedBlocks;
    }

    public void RecoverMe(List<(int, int)> blockCoords)
    {
        foreach ((int x, int y) coords in blockCoords)
        {
            GameManager.instance.SetMap(coords.x, coords.y, true);
        }
    }
}
