using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Auth;
using System;
using UnityEngine.SceneManagement;

public class AuthBehaviour : MonoBehaviour
{
    [SerializeField] InputField emailField;
    [SerializeField] InputField pwField;
    [SerializeField] Button joinBtn;
    [SerializeField] Button loginBtn;

    private FirebaseAuth auth; // 인증 객체 불러오기
    bool loginSuccess;

    void Start()
    {
        auth = FirebaseAuth.DefaultInstance;
        joinBtn.onClick.AddListener(Join);
        loginBtn.onClick.AddListener(Login);
    }

    private void Login()
    {
        auth.SignInWithEmailAndPasswordAsync(emailField.text, pwField.text).ContinueWith(
            task =>
            {
                if (task.IsCanceled)
                {
                    Debug.Log("SignInWithEmailAndPasswordAsync canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.Log("SignInWithEmailAndPasswordAsync error: " + task.Exception);
                    return;
                }
                FirebaseUser user = task.Result;
                Debug.LogFormat("User signed in successfully: {0} ({1})",
                    user.DisplayName, user.UserId);
                loginSuccess = true;
                PlayerPrefs.SetString("player_uid", auth.CurrentUser.UserId);
                PlayerPrefs.SetString("player_DisplayName", auth.CurrentUser.DisplayName);
            });

        StartCoroutine(LoadGameScene());
    }

    IEnumerator LoadGameScene()
    {
        yield return new WaitUntil(() => loginSuccess);
        SceneManager.LoadScene("Intro");
    }

    private void Join()
    {
        auth.CreateUserWithEmailAndPasswordAsync(emailField.text, pwField.text).ContinueWith(
             task => {
                 if (!task.IsCanceled && !task.IsFaulted)
                 {
                     Debug.Log("Sign up completed with " + emailField.text);
                 }
                 else
                 {
                     Debug.Log("Sign up failed");
                 }
             }
         );
    }

}
