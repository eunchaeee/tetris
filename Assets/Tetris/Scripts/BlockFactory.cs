using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockFactory : MonoBehaviour
{
    GameObject blockClone;
    [SerializeField] private List<GameObject> blocks = new List<GameObject>();
    public bool isFixed = false;

    private void OnEnable()
    {
        
    }
    private void Start()
    {
        StartCoroutine(CreateBlock());
    }

    IEnumerator CreateBlock()
    {
        while (true)
        {
            blockClone = Instantiate(blocks[Random.Range(0, blocks.Count)], transform, false);
            Block block = blockClone.GetComponent<Block>();
            block.isSelect = true;
            block.blockFactory = this;
            yield return new WaitUntil(() => isFixed);
            isFixed = false;
        }
    }
}
