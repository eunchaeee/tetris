using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{

    private int width = 10;
    private int height = 20;

    private bool[,] map;

    void Start()
    {
        InitializeMap();
    }

    private void InitializeMap()
    {
        map = new bool[width, height];
    }

    public bool GetMap(int i, int j)
    {
        return map[i, j];
    }

    public void SetMap(int i, int j, bool value)
    {
        map[i, j] = value;
    }

    public int GetMapHeight()
    {
        return height;
    }

    public int GetMapWidth()
    {
        return width;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(new Vector3(-5, +0.5f, 0), new Vector3(15, 0.5f, 0));
        for(int i = 0; i < width; i++)
        {
            for(int j = 0; j < height; j++)
            {
                Gizmos.color = Color.black;
                if(map != null && map[i, j])
                {
                    Gizmos.color = Color.white;
                }
                Gizmos.DrawSphere(new Vector3(i, j, 0), 0.2f);
            }
        }
    }
}
